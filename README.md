# OpenML dataset: California-Environmental-Conditions-Dataset

https://www.openml.org/d/43606

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Explore an environmental conditions dataframe scraped from CIMIS weather stations using a selenium chromedriver. With California's wildfires setting records in 2020, it is worthwhile to explore factors that may contribute to creating at risk environments. This dataset was used in conjunction to building an XGBoost Classifier to accurately predict probability for fire given environmental condition features. 
Following my Fire Risk Analysis project.
Content
262 Station Id's correspond to California weather station IDs. Approximately 14 numerical features for exploratory data analysis. Advanced users can keep date feature for time series analysis. Target column corresponds to fires on the respective observation date, in the observation region.
Acknowledgements

CIMIS: https://cimis.water.ca.gov/Default.aspx

Inspiration
What additional features would be valuable in determining fire risk?
What features are most important for specific models in determining target?
Is there an accurate LSTM to determine feature predictions?
" to determine fire risk in the future?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43606) of an [OpenML dataset](https://www.openml.org/d/43606). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43606/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43606/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43606/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

